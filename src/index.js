import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';

import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_KEY = "AIzaSyBWpxXnhC11UJXYokRtFiW2Qho0nPWSqmY";

class App extends Component {
    constructor (props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        // Load some initial data
        this.videoSearch('banana phone');
    }

    render() {
        // Create debounced version of video search so we won't slow down
        // the app by triggering a search on each key press
        const videoSearch = _.debounce(term => {this.videoSearch(term)}, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList
                    videos={this.state.videos}
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})} />
            </div>
        );
    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, videos => {
            this.setState({ videos , selectedVideo: videos[0]});
        });
    }
}


ReactDOM.render(<App />, document.querySelector('.container'));
