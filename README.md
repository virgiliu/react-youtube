## Simple YouTube search & view application

* Install dependencies `npm install`

* Get an YouTube API key from: https://console.developers.google.com/apis/api/youtube/overview

* Replace API_KEY in `src/index.js`

* Run server `npm start`

* Access application at http://localhost:8080

* :banana: :telephone_receiver: